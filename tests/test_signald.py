import re

from signald import Signal
from signald.types import Attachment
from signald.types import Message


def test_signal():
    """Smoke tests."""
    Signal("+1234567890")
    Message("+1234567890", "+1234567891", "Hello!")
    Attachment("application/json", "123", 123, "/var/123")


def test_bot():
    """Test the bot."""
    s = Signal("+1234567890")

    @s.chat_handler("simple message", order=10)
    def foo(message, match):
        pass

    @s.chat_handler(re.compile("compiled regex"), order=20)
    def bar(message, match):
        pass
